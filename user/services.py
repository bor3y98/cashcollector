from datetime import timedelta
from collections import deque
from django.contrib.auth import get_user_model
from django.conf import settings
from django.utils import timezone
from task.models import Task

User = get_user_model()


def check_user_collections(user):
    collected_tasks = Task.objects.filter(user=user, status=Task.TaskStatus.COLLECTED).order_by('collected_at')
    collected_amount = 0
    threshold_amount = settings.THRESHOLD_AMOUNT
    threshold_days = settings.THRESHOLD_DAYS
    print(threshold_days, 'days')
    print(threshold_amount, 'amoynt')
    tasks_list = deque([])
    for task in collected_tasks:
        tasks_list.append(task)
        collected_amount += task.amount
        print(collected_amount, 'collected_amount')
        print(collected_amount > threshold_amount)
        if collected_amount >= threshold_amount:
            datetime_difference = task.collected_at - timezone.now()
            if datetime_difference < timedelta(days=threshold_days):

                collected_amount -= tasks_list[0].amount
                tasks_list.popleft()
            else:
                user.is_frozen = True
                user.save(update_fields=['is_frozen'])
                break
    else:
        # will return false if no breaks, which means he didn't collect more than threshold limits
        # it will not reach this part if it breaks
        # so I check again if user was frozen, since it didn't break he should be unfrozen
        if user.is_frozen:
            user.is_frozen = False
            user.save(update_fields=['is_frozen'])
        return False
    # will return true if breaks, which means he is still frozen
    return True
