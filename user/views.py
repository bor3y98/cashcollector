from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from user.services import check_user_collections


# Create your views here.

class UserStatus(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        user = request.user
        check_user_collections(user)
        return Response({'is_frozen': user.is_frozen}, status=status.HTTP_200_OK)
