from django.contrib.auth.models import AbstractUser
from django.db import models
from user.mixin import Base


# Create your models here.
class User(AbstractUser):
    class UserRole(models.TextChoices):
        MANAGER = 'manager', 'manager'
        COLLECTOR = 'collector', 'collector'

    role = models.CharField(choices=UserRole.choices, default=UserRole.COLLECTOR, max_length=255)
    is_frozen = models.BooleanField(default=False)


class Customer(Base):
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
