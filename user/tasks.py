from celery import shared_task
from django.contrib.auth import get_user_model
from user.services import check_user_collections

User = get_user_model()


@shared_task
def check_users():
    cash_collectors = User.objects.filter(role=User.UserRole.COLLECTOR)
    for user in cash_collectors:
        check_user_collections(user)
