from django.test import TestCase, override_settings
from django.utils import timezone
from datetime import timedelta
from unittest.mock import patch
from django.conf import settings
from task.models import Task
from task.factories import UserFactory, TaskFactory
from user.models import User
from user.services import check_user_collections


def create_task(user, amount, collected_at, status):
    return TaskFactory(user=user, amount=amount, collected_at=collected_at, status=status)


@override_settings(THRESHOLD_AMOUNT=5000)
@override_settings(THRESHOLD_DAYS=2)
class UserCollectionsTest(TestCase):
    def test_user_collections_frozen(self):
        # Set up user and tasks
        user = UserFactory()
        threshold_amount = settings.THRESHOLD_AMOUNT
        threshold_days = settings.THRESHOLD_DAYS

        # Create tasks with amounts greater than the threshold
        now = timezone.now()
        yesterday = now - timedelta(days=1)
        task1 = create_task(user, amount=threshold_amount + 100, collected_at=yesterday,
                            status=Task.TaskStatus.COLLECTED)
        task2 = create_task(user, amount=threshold_amount + 200, collected_at=yesterday,
                            status=Task.TaskStatus.COLLECTED)
        delivered_task = create_task(user, amount=threshold_amount + 200, collected_at=yesterday,
                                     status=Task.TaskStatus.DELIVERED)

        # Mock timezone.now to return a date outside the threshold days
        frozen_date = now - timedelta(days=threshold_days + 1)
        with patch('django.utils.timezone.now', return_value=frozen_date):
            # Check user collections
            result = check_user_collections(user)
            print(result)
        user.refresh_from_db()
        # Assert that the user is frozen
        self.assertTrue(result)
        self.assertTrue(user.is_frozen)

    def test_user_collections_not_frozen(self):
        # Set up user and tasks
        user = UserFactory(is_frozen=True)
        threshold_amount = settings.THRESHOLD_AMOUNT
        threshold_days = settings.THRESHOLD_DAYS

        # Create tasks with amounts less than the threshold
        now = timezone.now()
        yesterday = now - timedelta(days=1)
        task1 = create_task(user, amount=threshold_amount - 100, collected_at=yesterday,
                            status=Task.TaskStatus.COLLECTED)
        task2 = create_task(user, amount=threshold_amount - 200, collected_at=yesterday,
                            status=Task.TaskStatus.COLLECTED)

        # Mock timezone.now to return a date within the threshold days
        unfrozen_date = now - timedelta(days=threshold_days - 1)
        with patch('django.utils.timezone.now', return_value=unfrozen_date):
            # Check user collections
            result = check_user_collections(user)

        # Assert that the user is not frozen
        self.assertFalse(result)
        self.assertFalse(user.is_frozen)

    def test_user_collections_exceed_amount_not_days(self):
        # Set up user and tasks
        user = User.objects.create(username='test_user')
        threshold_amount = settings.THRESHOLD_AMOUNT
        threshold_days = settings.THRESHOLD_DAYS

        # Create tasks with amounts greater than the threshold
        now = timezone.now()
        yesterday = now - timedelta(days=1)
        task1 = create_task(user, amount=threshold_amount + 100, collected_at=yesterday,
                            status=Task.TaskStatus.COLLECTED)
        task2 = create_task(user, amount=threshold_amount + 200, collected_at=yesterday,
                            status=Task.TaskStatus.COLLECTED)

        # Mock timezone.now to return a date within the threshold days
        unfrozen_date = now - timedelta(days=threshold_days - 1)
        with patch('django.utils.timezone.now', return_value=unfrozen_date):
            # Check user collections
            result = check_user_collections(user)

        # Assert that the user is not frozen
        self.assertFalse(result)
        self.assertFalse(user.is_frozen)
