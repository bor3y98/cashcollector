from django.urls import path
from user.views import UserStatus

urlpatterns = [
    path('status/', UserStatus.as_view(), name='user_status')
]
