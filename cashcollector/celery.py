import os

from celery import Celery

from celery.schedules import crontab

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cashcollector.settings")

app = Celery("cashcollector")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()


app.conf.beat_schedule = {
    "freeze_user": {
        "task": "user.tasks.freeze_user",
        "schedule": crontab(hour="*"),
    },
}


@app.task(bind=True)
def debug_task(self):
    print(f"Request: {self.request!r}")
