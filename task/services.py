from datetime import datetime
from django.shortcuts import get_object_or_404
from rest_framework import exceptions
from task.models import Task
from task.tasks import celery_check_cash_collectors


class TaskService:
    @staticmethod
    def get_first_task_pending_for_user(user):
        return Task.objects.filter(user=user, status=Task.TaskStatus.PENDING).order_by('due_at').first()

    def get_next_task(self, user):
        task = self.get_first_task_pending_for_user(user)
        return task

    def collect_task(self, user, pk):
        task = get_object_or_404(Task, id=pk, status=Task.TaskStatus.PENDING, user=user)
        first_task_pending = self.get_first_task_pending_for_user(user)
        if first_task_pending and task.id is not first_task_pending.id:
            raise exceptions.NotFound("task doesn't exists")
        celery_check_cash_collectors.delay(user.id)
        return self.update_task_for_collection(task)

    @staticmethod
    def update_task_for_collection(task):
        task.status = Task.TaskStatus.COLLECTED
        task.collected_at = datetime.now()
        task.save(update_fields=['status', 'collected_at', 'updated_at'])
        return task

    def deliver_task(self, user, pk):
        task = get_object_or_404(Task, id=pk, status=Task.TaskStatus.COLLECTED, user=user)
        task = self.update_task_for_delivery(task)
        celery_check_cash_collectors.delay(user.id)
        return task

    @staticmethod
    def update_task_for_delivery(task):
        task.status = Task.TaskStatus.DELIVERED
        task.delivered_at = datetime.now()
        task.save(update_fields=['status', 'delivered_at', 'updated_at'])
        return task
