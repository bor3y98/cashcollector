from django.contrib.auth import get_user_model
from rest_framework.permissions import BasePermission

User = get_user_model()


class NotFrozen(BasePermission):

    def has_permission(self, request, view):
        return not request.user.is_frozen


class IsManager(BasePermission):
    def has_permission(self, request, view):
        return request.user.role == User.UserRole.MANAGER
