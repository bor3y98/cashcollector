from rest_framework import serializers
from task.models import Task, Customer


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = '__all__'


class TaskSerializer(serializers.ModelSerializer):
    customer = CustomerSerializer()

    class Meta:
        model = Task
        fields = '__all__'
