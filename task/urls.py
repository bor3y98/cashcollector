from django.urls import include, path
from task.views import *
from rest_framework import routers

router = routers.DefaultRouter()

router.register('tasks', CollectedTasksList, basename="collected-tasks")
router.register('', TaskViewSet, basename="tasks-view-set")
router.register('all-tasks', TasksList, basename="tasks-list")

urlpatterns = [
    path("", include(router.urls))
]
