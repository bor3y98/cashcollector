from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet, GenericViewSet
from task.models import Task
from task.permissions import NotFrozen, IsManager
from task.serializers import TaskSerializer
from task.services import TaskService


# Create your views here.
class CollectedTasksList(ReadOnlyModelViewSet):
    serializer_class = TaskSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Task.objects.filter(user=self.request.user).exclude(status=Task.TaskStatus.PENDING).select_related(
            'customer')


class TasksList(ReadOnlyModelViewSet):
    serializer_class = TaskSerializer
    permission_classes = [IsAuthenticated, IsManager]

    def get_queryset(self):
        return Task.objects.all().select_related(
            'customer')


class TaskViewSet(GenericViewSet):
    permission_classes = [IsAuthenticated]

    @action(methods=['get'], detail=False)
    def next_task(self, request, *args, **kwargs):
        task = TaskService().get_next_task(request.user)
        if not task:
            return Response({'message': 'There are no tasks pending for this user'}, status=status.HTTP_200_OK)
        serialized_data = TaskSerializer(task).data
        return Response(serialized_data, status=status.HTTP_200_OK)

    @action(methods=['post'], detail=True, permission_classes=[IsAuthenticated, NotFrozen])
    def collect(self, request, *args, **kwargs):
        TaskService().collect_task(request.user, kwargs.get('pk'))
        return Response({'message': 'collected'}, status=status.HTTP_200_OK)

    @action(methods=['post'], detail=True)
    def pay(self, request, *args, **kwargs):
        TaskService().deliver_task(request.user, kwargs.get('pk'))
        return Response({'message': 'delivered'}, status=status.HTTP_200_OK)
