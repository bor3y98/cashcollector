from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from task.models import Task
from task.factories import UserFactory, TaskFactory, CustomerFactory
from django.utils import timezone
from datetime import timedelta


class CollectedTasksListTests(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = UserFactory()
        self.user_1 = UserFactory()
        self.client.force_authenticate(user=self.user)
        self.customer = CustomerFactory()
        self.collected_task = TaskFactory(user=self.user, customer=self.customer, status=Task.TaskStatus.COLLECTED)
        self.pending_task = TaskFactory(user=self.user, customer=self.customer, status=Task.TaskStatus.PENDING)
        self.collected_task_user_1 = TaskFactory(user=self.user_1, customer=self.customer,
                                                 status=Task.TaskStatus.PENDING)
        self.url = reverse('collected-tasks-list')

    def test_get_collected_tasks(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('results', response.data)
        self.assertEqual(len(response.data['results']), 1)
        self.assertEqual(response.data['results'][0]['status'], Task.TaskStatus.COLLECTED)


class TasksListTests(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.manager = UserFactory(role='manager')
        self.collector = UserFactory(role='collector')
        self.client.force_authenticate(user=self.manager)
        self.customer = CustomerFactory()
        self.task = TaskFactory(customer=self.customer)
        self.url = reverse('tasks-list-list')

    def test_get_all_tasks(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('results', response.data)
        self.assertEqual(len(response.data['results']), 1)

    def test_get_all_tasks_collector_user(self):
        self.client.force_authenticate(user=self.collector)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class TaskViewSetTests(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = UserFactory(is_frozen=False)
        self.frozen_user = UserFactory(is_frozen=True)
        self.client.force_authenticate(user=self.user)
        self.customer = CustomerFactory()
        specific_due_at = timezone.now() + timedelta(days=5)
        self.task = TaskFactory(user=self.user, customer=self.customer, status=Task.TaskStatus.PENDING,
                                due_at=specific_due_at)
        specific_due_at = timezone.now() + timedelta(days=10)
        self.task_2 = TaskFactory(user=self.user, customer=self.customer, status=Task.TaskStatus.PENDING,
                                  due_at=specific_due_at)

    def test_get_next_task(self):
        url = reverse('tasks-view-set-next-task')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('id', response.data)
        self.assertEqual(response.data['id'], self.task.id)
        self.assertEqual(response.data['status'], Task.TaskStatus.PENDING)

    def test_collect_task(self):
        url = reverse('tasks-view-set-collect', kwargs={'pk': self.task.id})
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.task.refresh_from_db()
        self.assertEqual(self.task.status, Task.TaskStatus.COLLECTED)

    def test_collect_task_not_next_pending(self):
        url = reverse('tasks-view-set-collect', kwargs={'pk': self.task_2.id})
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_collect_task_frozen_user(self):
        self.client.force_authenticate(user=self.frozen_user)
        url = reverse('tasks-view-set-collect', kwargs={'pk': self.task.id})
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_deliver_task(self):
        # First collect the task
        self.task.status = Task.TaskStatus.COLLECTED
        self.task.save()
        url = reverse('tasks-view-set-pay', kwargs={'pk': self.task.id})
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.task.refresh_from_db()
        self.assertEqual(self.task.status, Task.TaskStatus.DELIVERED)

    def test_deliver_task_invalid_status(self):
        self.task.status = Task.TaskStatus.PENDING
        self.task.save()
        url = reverse('tasks-view-set-pay', kwargs={'pk': self.task.id})
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
