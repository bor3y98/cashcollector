from django.db import models
from django.utils import timezone
from django.contrib.auth import get_user_model
from user.models import Customer
from user.mixin import Base

User = get_user_model()


class Task(Base):
    class TaskStatus(models.TextChoices):
        COLLECTED = 'collected', 'collected'
        DELIVERED = 'delivered', 'delivered'
        PENDING = 'pending', 'pending'

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='tasks')
    amount = models.DecimalField(max_digits=15, decimal_places=2)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, related_name='tasks')
    # I really don't know what do with this field, but it's listed in task descriptions
    due_at = models.DateTimeField(default=timezone.now)
    status = models.CharField(max_length=255, choices=TaskStatus.choices)
    delivered_at = models.DateTimeField(null=True, blank=True)
    collected_at = models.DateTimeField(null=True, blank=True)
