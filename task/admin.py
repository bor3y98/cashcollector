from django.contrib import admin
from task.models import Task, Customer

# Register your models here.

admin.site.register(Customer)
admin.site.register(Task)
