import factory
from django.utils import timezone
from django.contrib.auth import get_user_model
from user.models import Customer
from task.models import Task

User = get_user_model()


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.Faker('user_name')
    email = factory.Faker('email')
    role = factory.Iterator([User.UserRole.MANAGER, User.UserRole.COLLECTOR])
    is_frozen = factory.Faker('boolean')
    password = factory.PostGenerationMethodCall('set_password', '123')  # Set a default password


class CustomerFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Customer

    name = factory.Faker('name')
    address = factory.Faker('address')


class TaskFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Task

    user = factory.SubFactory(UserFactory)
    amount = factory.Faker('pydecimal', left_digits=13, right_digits=2, positive=True)
    customer = factory.SubFactory(CustomerFactory)
    due_at = factory.Faker('future_datetime', end_date="+30d", tzinfo=timezone.get_current_timezone())
    status = factory.Iterator([Task.TaskStatus.COLLECTED, Task.TaskStatus.DELIVERED, Task.TaskStatus.PENDING])

    @factory.lazy_attribute
    def collected_at(self):
        if self.status == Task.TaskStatus.COLLECTED:
            naive_datetime = factory.Faker('past_datetime', start_date="-30d").evaluate(
                None, None, {'locale': None})
            return timezone.make_aware(naive_datetime, timezone=timezone.get_current_timezone())
        return None

    @factory.lazy_attribute
    def delivered_at(self):
        if self.status == Task.TaskStatus.DELIVERED:
            naive_datetime = factory.Faker('past_datetime', start_date="-30d").evaluate(
                None, None, {'locale': None})
            return timezone.make_aware(naive_datetime, timezone=timezone.get_current_timezone())
        return None
