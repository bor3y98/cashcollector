from celery import shared_task
from django.contrib.auth import get_user_model
from user.services import check_user_collections

User = get_user_model()


@shared_task()
def celery_check_cash_collectors(user_id):
    user = User.objects.filter(user_id=user_id).first()
    if user:
        check_user_collections(user)
