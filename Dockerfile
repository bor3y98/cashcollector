FROM python:3.10-slim-bullseye

# Install required system dependencies
RUN apt-get update && apt-get install --no-install-recommends -y \
  build-essential \
  libpq-dev \
  libpq5 \
  libmagic1 \
  git \
  && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
  && rm -rf /var/lib/apt/lists/*

# Install Python Requirements
COPY ./req.txt /req.txt
RUN pip install --no-cache-dir -r req.txt

WORKDIR /app

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1

COPY ./start /start
RUN chmod +x /start
COPY . /app/

CMD ["/start"]
