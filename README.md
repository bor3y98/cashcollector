# Django App with Docker

This repository contains a Django application set up to run with Docker. Follow the instructions below to get started.

## Prerequisites

Make sure you have Docker installed on your system. If not, you can download and install it from [Docker's official website](https://www.docker.com/get-started).

## Setup

1. Clone this repository to your local machine:

    ```bash
    git clone <repository-url>
    ```

2. Navigate to the project directory:

    ```bash
    cd <repository-directory>
    ```

3. Create a `.env` file based on `dev.env` and update the values according to your environment.

## Usage

1. Build the Docker images:

    ```bash
    docker-compose build
    ```

2. Start the Docker containers:

    ```bash
    docker-compose up
    ```

3. Access the Django application in your web browser at [http://localhost:8000](http://localhost:8000).

## Notes

- Make sure no other service is running on port 8000 as it will conflict with the Django application.

- If you need to make changes to the Django application, you can do so in the `src` directory. The changes will be automatically reflected when you restart the Docker containers.

- For production deployment, make sure to configure your Docker environment accordingly.
